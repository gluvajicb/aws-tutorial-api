﻿namespace SecureDocuments.Presentation.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using SecureDocuments.BLL.Interfaces.Services;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
    }
}
