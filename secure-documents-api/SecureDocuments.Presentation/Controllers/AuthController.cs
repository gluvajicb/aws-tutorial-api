﻿namespace SecureDocuments.Presentation.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using SecureDocuments.BLL.Interfaces.Services;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }
    }
}
