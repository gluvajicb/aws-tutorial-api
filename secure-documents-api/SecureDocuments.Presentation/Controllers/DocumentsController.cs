﻿namespace SecureDocuments.Presentation.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using SecureDocuments.BLL.Interfaces.Services;

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private IDocumentService _documentService;

        public DocumentsController(IDocumentService documentService)
        {
            _documentService = documentService;
        }
    }
}
