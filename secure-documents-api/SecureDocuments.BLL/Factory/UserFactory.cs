﻿namespace SecureDocuments.BLL.Factory
{
    using SecureDocuments.BLL.Dto;
    using SecureDocuments.BLL.Entities;
    using System.Linq;

    public static class UserFactory
    {
        #region DB to Domain
        
        public static User ToEntityUser(this DAL.Model.User dbUser)
        {
            return new User(dbUser.Id,
                            dbUser.Uid,
                            dbUser.Email,
                            dbUser.Password,
                            dbUser.ResetToken,
                            dbUser.CreatedAt,
                            dbUser.DeletedAt,
                            dbUser.Document.Select(e => e.ToEntityDocument()).ToList());
        }

        #endregion

        #region DB to Dto

        public static UserDto ToDtoUser(this DAL.Model.User dbUser)
        {
            return new UserDto(dbUser.Uid,
                               dbUser.Email);
        }

        #endregion

        #region Domain to DB

        public static DAL.Model.User ToDbUser(this User entityUser)
        {
            return new DAL.Model.User(entityUser.Id,
                                      entityUser.Uid,
                                      entityUser.Email,
                                      entityUser.Password,
                                      entityUser.ResetToken,
                                      entityUser.CreatedAt,
                                      entityUser.DeletedAt);
        }

        #endregion

        #region Domain to Dto

        public static UserDto ToDtoUser(this User entityUser)
        {
            return new UserDto(entityUser.Uid,
                               entityUser.Email);
        }

        #endregion
    }
}
