﻿namespace SecureDocuments.BLL.Services
{
    using Microsoft.Extensions.Configuration;
    using SecureDocuments.BLL.Interfaces.Services;
    using SecureDocuments.BLL.Registries;

    public class AuthService : IAuthService
    {
        private string _serverPrivateKey;

        public AuthService()
        {
            _serverPrivateKey = ConfigurationRegistry.GetInstance().Configuration.GetValue<string>("AppSettings:ServerSecret");

            
        }
    }
}
