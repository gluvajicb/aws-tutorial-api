﻿namespace SecureDocuments.DAL.Interfaces.Repository
{
    using SecureDocuments.DAL.Model;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IDocumentRepository
    {
        Guid Create(Document document);

        Task<List<Document>> GetAll();

        Task<List<Document>> GetByUserId(int userId);

        Task<Document> GetById(Guid id);

        Document Update(Document document);

        void Delete(Guid id);
    }
}
