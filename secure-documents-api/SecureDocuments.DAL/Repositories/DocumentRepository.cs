﻿namespace SecureDocuments.DAL.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using SecureDocuments.DAL.Interfaces.Repository;
    using SecureDocuments.DAL.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class DocumentRepository : IDocumentRepository
    {
        private SecureDocumentsContext _context;
        private DbSet<Document> _collection;

        public Guid Create(Document document)
        {
            document.CreatedAt = DateTime.UtcNow;

            _collection.Add(document);
            _context.SaveChanges();

            return document.Uid;
        }

        public async void Delete(Guid uid)
        {
            var found = await _collection.FirstOrDefaultAsync(e => e.Uid == uid);

            if (found != null)
            {
                _collection.Remove(found);
                _context.SaveChanges();
            }
        }

        public async Task<List<Document>> GetAll()
        {
            return await _collection.Include(e => e.User).AsNoTracking().ToListAsync();
        }

        public async Task<Document> GetById(Guid uid)
        {
            return await _collection.AsNoTracking().FirstOrDefaultAsync(e => e.Uid == uid);
        }

        public async Task<List<Document>> GetByUserId(int userId)
        {
            return await _collection.Include(e => e.User).Where(e => e.User.Id == userId).AsNoTracking().ToListAsync();
        }

        public Document Update(Document document)
        {
            _context.Entry(document).State = EntityState.Modified;
            _context.SaveChanges();

            return document;
        }
    }
}
