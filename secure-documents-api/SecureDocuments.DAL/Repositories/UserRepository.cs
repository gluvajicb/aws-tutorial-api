﻿namespace SecureDocuments.DAL.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using SecureDocuments.DAL.Interfaces.Repository;
    using SecureDocuments.DAL.Model;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class UserRepository : IUserRepository
    {
        private SecureDocumentsContext _context;
        private DbSet<User> _collection;

        public Guid Create(User user)
        {
            user.CreatedAt = DateTime.UtcNow;

            _collection.Add(user);
            _context.SaveChanges();

            return user.Uid;
        }

        public async void Delete(Guid uid)
        {
            var found = await _collection.FirstOrDefaultAsync(e => e.Uid == uid);

            if (found != null)
            {
                _collection.Remove(found);
                _context.SaveChanges();
            }
        }

        public Task<List<User>> GetAll()
        {
            return _collection.Include(e => e.Document).AsNoTracking().ToListAsync();
        }

        public async Task<User> GetById(Guid uid)
        {
            return await _collection.AsNoTracking().FirstOrDefaultAsync(e => e.Uid == uid);
        }

        public User Update(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();

            return user;
        }
    }
}
